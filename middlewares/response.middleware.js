const responseMiddleware = (req, res, next) => {
    const { err, data } = res;
    if(!err){
        res.status(200).send(data);
        return;
    }
    const { status = 500, message } = err;
    res.status(status).send({
        error: true,
        message: message || err,
    });
    return;
}

exports.responseMiddleware = responseMiddleware;